package Modelo;
public class Pessoa{

	private String nome;
	private String idade;
	private String telefone;
	private String sexo;
	private String email;
	private String hangout;
	private String endereco;
	private String rg;
	private String cpf;



	public Pessoa(String umNome){
		nome = umNome;
	}

	public Pessoa(String umNome, String umTelefone){
		nome = umNome;
		telefone = umTelefone;
	}
        
	public Pessoa(){
	}


	public void setNome(String umNome){
		nome = umNome;
	}

	public String getNome(){
		return nome;
	}

	public void setIdade(String umaIdade){
		idade = umaIdade;
	}
        
        public void setTelefone(String umTelefone){
		telefone = umTelefone;
	}

	public String getTelefone(){
		return telefone;
	}
	public String getIdade(){
		return idade;
	}

	public void setSexo(String umSexo){
		sexo = umSexo;
	}

	public String getSexo(){
		return sexo;
	}

	public void setEmail(String umEmail){
		email = umEmail;
	}

	public String getEmail(){
		return email;
	}

	public void setHangout(String umHangout){
		hangout = umHangout;
	}

	public String getHangout(){
		return hangout;
	}

	public void setEndereco(String umEndereco){
		endereco = umEndereco;
	}

	public String getEndereco(){
		return endereco;
	}

	public void setRg(String umRg){
		rg = umRg;
	}

	public String getRg(){
		return rg;
	}

	public void setCpf(String umCpf){
		cpf = umCpf;
	}

	public String getCpf(){
		return cpf;
	}
}
