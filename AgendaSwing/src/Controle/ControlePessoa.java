package Controle;
import Modelo.Pessoa;
import java.util.ArrayList;

public class ControlePessoa {


    private ArrayList<Pessoa> listaPessoas;


    public ControlePessoa() {
        listaPessoas = new ArrayList<Pessoa>();
    }
    

   
    public void adicionar(Pessoa umaPessoa) {
		listaPessoas.add(umaPessoa);
    }

    public void remover(Pessoa umaPessoa) {
        listaPessoas.remove(umaPessoa);
    }
    
    public Pessoa pesquisarNome(String umNome) {
        for (Pessoa umaPessoa: listaPessoas) {
            if (umaPessoa.getNome().equalsIgnoreCase(umNome)) return umaPessoa;
        }
        return null;
    }
    
    public void exibirContatos () {
        for (Pessoa umaPessoa : listaPessoas) {
            System.out.println(umaPessoa.getNome());
        }
    }

}